import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LOGOUTComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path:'',        component:LoginComponent},
  {path:'Login',   component:LoginComponent},
  {path:'Register', component:RegisterComponent},
  {path:'Home', component:HomeComponent},
  {path:'AboutUs',  component:AboutusComponent},
  {path:'logout',   component:LOGOUTComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
