import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent {
  menuItems = [
    { category: 'Coffee', items: [
      { name: 'Filter Coffee', price: 99 },
      { name: 'Chai Latte', price: 109 },
      { name: 'Masala Coffee', price: 89 },
      { name: 'Turmeric Latte', price: 89 },
      { name: 'Cardamom Coffee', price: 119 },
      { name: 'Ginger Coffee', price: 99 },
      { name: 'Chocolate Chai', price: 119 },
      { name: 'Coconut Coffee', price: 119 },
      { name: 'Hazelnut Chai', price: 139 },
      { name: 'Mint Coffee', price: 89 },
      { name: 'Vanilla Chai', price: 129 },
      { name: 'Cinnamon Coffee', price: 109 },
      { name: 'Almond Chai', price: 99 },
      { name: 'Rose Coffee', price: 109 },
      { name: 'Saffron Latte', price: 119 },
      {name:'Americano',price:149},
      {name:'Iced Coffee',price:129},
      {name:'Cappuccino',price:149},
      // Add more coffee items as needed
    ]},
    { category: 'Desserts', items: [
      { name: 'Chocolate Cake', price: 119 },
      { name: 'Cheesecake', price: 89},
      { name: 'Tiramisu', price: 249 },
      { name: 'Gulab Jamun', price: 240 },
      { name: 'Jalebi', price: 159 },
      { name: 'Rasgulla', price: 329},
      { name: 'Kheer', price: 369 },
      { name: 'Halwa', price: 278 },
      { name: 'Ladoo', price: 289 },
      { name: 'Barfi', price: 300 },
      { name: 'Rasmalai', price: 450 },
      { name: 'Gajar Ka Halwa', price: 460},
      { name: 'Mysore Pak', price: 250 },
      { name: 'Malpua', price: 280},
      { name: 'Shahi Tukda', price: 189},
      { name: 'Sandesh', price: 190 },
      { name: 'Cham Cham', price: 200},
      { name: 'Peda', price: 350 },
    ]},
    { category: 'Snacks', items: [
      { name: 'French Fries', price: 300 },
      { name: 'Chicken Wings', price: 249 },
      { name: 'Nachos', price: 249 },
      { name: 'Samosa', price: 129 },
      { name: 'Pakora', price: 129 },
      { name: 'Bhel Puri', price: 99 },
      { name: 'Aloo Tikki', price: 109 },
      { name: 'Pani Puri', price: 79 },
      { name: 'Dahi Puri', price: 89 },
      { name: 'Vada Pav', price: 89 },
      { name: 'Paneer Tikka', price: 249 },
      { name: 'Dhokla', price: 129 },
      { name: 'Chaat Papri', price: 109 },
      { name: 'Kachori', price: 89 },
      { name: 'Khandvi', price: 119 },
      { name: 'Aloo Chaat', price: 99 },
      { name: 'Murmura Chivda', price: 79 },
      { name: 'Masala Puri', price: 89 },
    ]},
    { category: 'Mocktails', items: [
      { name: 'Virgin Mojito', price: 209 },
      { name: 'Berry Blast', price: 199 },
      { name: 'Tropical Punch', price: 189 },
      { name: 'Masala Lemonade', price: 199 },
      { name: 'Spiced Buttermilk', price: 109 },
      { name: 'Rose Sharbat', price: 400 },
      { name: 'Thandai', price: 100 },
      { name: 'Kokum Sherbet', price: 150 },
      { name: 'Cucumber Mint Cooler', price: 149 },
      { name: 'Ginger Fizz', price: 119 },
      { name: 'Aam Panna', price: 109 },
      { name: 'Chai Masala Smoothie', price: 119 },
      { name: 'Strawberry Daiquiri', price: 189 },
      { name: 'Mint Julep', price: 119 },
      { name: 'Mai Tai', price: 109 },
      { name: 'Margarita', price: 199 },
      { name: 'Blue Lagoon', price: 199 },
      { name: 'Mint Lemonade', price: 119 },
      
    ]},
    // Add more categories as needed
    { category: 'Mains', items: [
      { name: 'Paneer Butter Masala', price: 249 },
      { name: 'Chicken Curry', price: 249 },
      { name: 'Dal Tadka', price: 199 },
      { name: 'Biryani', price: 299 },
      { name: 'Butter Chicken', price: 329 },
      { name: 'Chole Bhature', price: 109 },
      { name: 'Palak Paneer', price: 199 },
      { name: 'Rajma Chawal', price: 109 },
      { name: 'Aloo Gobi', price: 249 },
      { name: 'Fish Curry', price: 359 },
      { name: 'Mutton Curry', price: 449 },
      { name: 'Vegetable Biryani', price: 229 },
      { name: 'Chicken Biryani', price: 319 },
      { name: 'Samosa Chaat', price: 109 },
      { name: 'Mushroom Masala', price: 229 },
      { name: 'Tandoori Roti', price: 49 },
      { name: 'Naan', price: 39 },
      { name: 'Chicken Korma', price: 229 },
     
  ]},
    
   ]}