package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.UserInfo;

@Service
public class UserInfoDao {

	@Autowired
	UserInfoRepsoitory userRepo;
	
	
	public UserInfo userLogin(String emailId, String password) {
		return userRepo.userLogin(emailId, password);
	}

	public List<UserInfo> getAllUserInfo() {
		return userRepo.findAll();
	}

	public UserInfo getUserInfoById(int userId) {
		return userRepo.findById(userId).orElse(null);
	}

	public List<UserInfo> getUserInfoByName(String userName) {
		return userRepo.findByName(userName);
	}
	
	public UserInfo addUserInfo(UserInfo user) {
		System.out.println(user.getPassword());
		
		return userRepo.save(user);
	}
	
	public UserInfo updateUserInfo(UserInfo user) {
		return userRepo.save(user);
	}

	public void deleteUserInfoById(int userId) {
		userRepo.deleteById(userId);
	}

	public UserInfo getUserInfoByEmail(String emailId) {
		// TODO Auto-generated method stub
		return userRepo.getByEmail(emailId);
		
	}
//	public UserInfo udateUserInfoByEmail(String email,String password){
//		return userRepo.udateUserInfoByEmail(email,password);
//	}
	
}