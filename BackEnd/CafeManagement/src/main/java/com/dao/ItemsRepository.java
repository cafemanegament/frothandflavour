package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Items;

@Repository
public interface ItemsRepository extends JpaRepository<Items, Integer> {

	@Query("from Items e where e.name = :name")
	List<Items> findByName(@Param("name") String itemsName);

}