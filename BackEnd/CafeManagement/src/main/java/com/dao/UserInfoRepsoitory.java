package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.UserInfo;

@Repository
public interface UserInfoRepsoitory extends JpaRepository<UserInfo, Integer> {

	@Query("from UserInfo e where e.userName = :name")
	List<UserInfo> findByName(@Param("name") String userName);

	@Query("from UserInfo e where e.emailId = :emailId and e.password = :password")
	UserInfo userLogin(@Param("emailId") String emailId, @Param("password") String password);

//	void getByEmail(String emailId);
	@Query("from UserInfo e where e.emailId = :emailId ")
	UserInfo getByEmail(@Param("emailId") String emailId);

//	@Query("update UserInfo set password=:password where emailId=:emailId;")
//	UserInfo udateUserInfoByEmail(@Param("emailId") String emailId, @Param("password") String password);
}
