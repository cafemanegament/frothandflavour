package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Items;

@Service
public class ItemsDao {

	@Autowired
	ItemsRepository itemsRepo;
	

	public List<Items> getAllItems() {
		return itemsRepo.findAll();
	}

	public Items getItemsById(int itemsId) {
		return itemsRepo.findById(itemsId).orElse(null);
	}

	public List<Items> getItemsByName(String itemsName) {
		return itemsRepo.findByName(itemsName);
	}

	public Items addItems(Items items) {
		return itemsRepo.save(items);
	}
	
	public Items updateItems(Items items) {
		return itemsRepo.save(items);
	}

	public void deleteItemsById(int itemsId) {
		itemsRepo.deleteById(itemsId);
	}
	
}