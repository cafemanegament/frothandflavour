package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ItemsDao;
import com.model.Items;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class ItemsController {
	
	@Autowired
	ItemsDao itemsDao;
	@GetMapping("getAllItems")
	public List<Items> getAllItems() {
		return itemsDao.getAllItems();
	}
	@GetMapping("getItemsById/{id}")
	public Items getItemsById(@PathVariable("id") int itemsId) {
		return itemsDao.getItemsById(itemsId);
	}
	
	@GetMapping("getItemsByName/{name}")
	public List<Items> getItemsByName(@PathVariable("name") String itemsName) {
		return itemsDao.getItemsByName(itemsName);
	}
	
	@PostMapping("addItems")
	public Items addItems(@RequestBody Items items) {
		return itemsDao.addItems(items);
	}
	
	@PutMapping("updateItems")
	public Items updateItems(@RequestBody Items items) {
		return itemsDao.updateItems(items);
	}
	
	@DeleteMapping("deleteItemsById/{id}")
	public String deleteItemsById(@PathVariable("id") int itemsId) {
		itemsDao.deleteItemsById(itemsId);
		return "Product Record Deleted Successfully!!!";
	}
}