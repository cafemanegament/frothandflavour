package com.controllers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserInfoDao;
import com.model.UserInfo;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class UserInfoController {
	
	@Autowired
	UserInfoDao userDao;
	
	@GetMapping("userLogin/{emailId}/{password}")
	public UserInfo userLogin(@PathVariable("emailId") String emailId, 
			@PathVariable("password") String password) {
		String encryptedpassword = null; 
		  try   
	        {  
	            /* MessageDigest instance for MD5. */  
	            MessageDigest m = MessageDigest.getInstance("MD5");  
	              
	            /* Add plain-text password bytes to digest using MD5 update(
	             * ) method. */  
	            m.update(password.getBytes());  
	              
	            /* Convert the hash value into bytes */   
	            byte[] bytes = m.digest();  
	              
	            /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */  
	            StringBuilder s = new StringBuilder();  
	            for(int i=0; i< bytes.length ;i++)  
	            {  
	                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
	            }  
	              
	            /* Complete hashed password in hexadecimal format */  
	            encryptedpassword = s.toString();  
	        }   
	        catch (NoSuchAlgorithmException e)   
	        {  
	            e.printStackTrace();  
	        }  
		
		return userDao.userLogin(emailId, encryptedpassword);
	}
	
	@GetMapping("getAllUserInfo")
	public List<UserInfo> getAllUserInfo() {
		return userDao.getAllUserInfo();
	}
	
	@GetMapping("getUserInfoById/{id}")
	public UserInfo getUserInfoById(@PathVariable("id") int userId) {
		return userDao.getUserInfoById(userId);
	}
	
	@GetMapping("getUserInfoByName/{name}")
	public List<UserInfo> getUserInfoByName(@PathVariable("name") String userName) {
		return userDao.getUserInfoByName(userName);
	}
	@GetMapping("getUserInfoByEmail/{emailId}")
	public UserInfo getUserInfoByEmail(@PathVariable("emailId") String emailId) {
		return (UserInfo) userDao.getUserInfoByName(emailId);
	}
	
	@PostMapping("addUserInfo")
	public UserInfo addUserInfo(@RequestBody UserInfo user) {
		return userDao.addUserInfo(user);
	}
	
	@PutMapping("updateUserInfo")
	public UserInfo updateUserInfo(@RequestBody UserInfo user) {
		return userDao.updateUserInfo(user);
	}
	
//	@PutMapping("updateUserInfo")
//	public UserInfo udateUserInfoByEmail(@PathVariable("emailId") String emailId, 
//			@PathVariable("password") String password) {
//		return userDao.udateUserInfoByEmail(emailId,password);
//	}
//	
	@DeleteMapping("deleteUserInfoById/{id}")
	public String deleteUserInfoById(@PathVariable("id") int userId) {
		userDao.deleteUserInfoById(userId);
		return "UserInfo Record Deleted Successfully!!!";
	}
}