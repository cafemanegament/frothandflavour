package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Items {
	@Id
	@GeneratedValue
	private int iId;
	private String name;
	private double price;
	private String description;
	private String imgsrc;

	public Items(int iId, String name, double price, String description, String imgsrc) {
		super();
		this.iId = iId;
		this.name = name;
		this.price = price;
		this.description = description;
		this.imgsrc = imgsrc;
	}

	public int getiId() {
		return iId;
	}

	public void setiId(int iId) {
		this.iId = iId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgsrc() {
		return imgsrc;
	}

	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}

	@Override
	public String toString() {
		return "Items [iId=" + iId + ", name=" + name + ", price=" + price + ", description=" + description
				+ ", imgsrc=" + imgsrc + "]";
	}
}
