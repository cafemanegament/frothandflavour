package com.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserInfo {

	@Id@GeneratedValue
	private int id;
	private String userName;
	private String country;
	private String address;
	private Date dob;
	
	@Column(unique = true)
	private String emailId;
	private String mobileNo;
	private String password;
	

	
	public UserInfo() {
		
		// TODO Auto-generated constructor stub
	}

	public UserInfo(int id, String userName, String country, String address, Date dob, String emailId, String mobileNo,
			String password) {
		
		this.id = id;
		this.userName = userName;
		this.country = country;
		this.address = address;
		this.dob = dob;
		this.emailId = emailId;
		this.mobileNo = mobileNo;
		this.password = password;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
//		String encrption="";
//		for(int i=0;i<password.length();i++){
//		    int n=password.charAt(i);
//		    n+=3;
//		    char a=(char)(n);
//
//			encrption+=a;
//		}
//		System.out.println(encrption);
		 String encryptedpassword = null; 
		  try   
	        {  
	            /* MessageDigest instance for MD5. */  
	            MessageDigest m = MessageDigest.getInstance("MD5");  
	              
	            /* Add plain-text password bytes to digest using MD5 update(
	             * ) method. */  
	            m.update(password.getBytes());  
	              
	            /* Convert the hash value into bytes */   
	            byte[] bytes = m.digest();  
	              
	            /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */  
	            StringBuilder s = new StringBuilder();  
	            for(int i=0; i< bytes.length ;i++)  
	            {  
	                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
	            }  
	              
	            /* Complete hashed password in hexadecimal format */  
	            encryptedpassword = s.toString();  
	        }   
	        catch (NoSuchAlgorithmException e)   
	        {  
	            e.printStackTrace();  
	        }  
		this.password =encryptedpassword;
	}
	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", userName=" + userName + ", country=" + country + ", address=" + address
				+ ", dob=" + dob + ", emailId=" + emailId + ", mobileNo=" + mobileNo + ", password=" + password + "]";
	}

	

	
	

	

	
}
